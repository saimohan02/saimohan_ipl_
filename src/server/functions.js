
const csv = require('csvtojson')
const deliveriesPath='../data/deliveries.csv'
const matchsPath = '../data/matches.csv'

csv()
.fromFile(matchsPath)
.then((matchsobj)=> {
    csv()
    .fromFile(deliveriesPath)
    .then((deliveriesObj)=>{
        economyOfTopTenBowlersIn2015(matchsobj,deliveriesObj);
        numberOfMatchesPerYear(matchsobj);
        numberOfWinsEachYearByAllTeams(matchsobj)
        extraRunsPerTeam(matchsobj,deliveriesObj)
    })
.catch((errorr)=>{
    console.log("error occured while reading deliveries.csv file")
    console.log(errorr)
})
})
.catch((errorrr)=>{
    console.log("error occured while reading matches.csv file ")
    console.error(errorrr);
})

// first function
function numberOfMatchesPerYear(matchsobj){
    console.log("ji")
          noOfMatchs ={};
        for(let index= 0;index< matchsobj.length;index++ )
            {  
      
            if(noOfMatchs[matchsobj[index].season ] === undefined)
            {
         noOfMatchs[matchsobj[index].season] = 1;
                }
           else
         {
         noOfMatchs[matchsobj[index].season] += 1 ;
          }
    }
     console.log('number of matchs');
     console.log(noOfMatchs);
     
const fs = require('fs');
let data = JSON.stringify(noOfMatchs, null, 2);
fs.writeFile('../public/output/numberOfMatchesPerYear.json', data, (err) => {
 if (err) throw err;
});
}
//second function
function numberOfWinsEachYearByAllTeams(matchsobj){
    let matchsArray =[];
    for(let index = 0;index< matchsobj.length;index++ )
    {
               flag =0;
      for(let j =0;j<matchsArray.length;j++)
      { 
          if(matchsArray[j][0] == [matchsobj[index].winner])
          {  
               if(matchsArray[j][1] == matchsobj[index].season)
               {    
                    flag =1;
                    matchsArray[j][2] +=1;  
               }
          }
      }

      if(flag == 0)
      {
      matchsArray[matchsArray.length]=[matchsobj[index].winner,matchsobj[index].season,1] ;
      }
   
     } 
matchsArray.sort();
console.log(matchsArray);

const fs = require('fs');
let data = JSON.stringify(matchsArray, null, 2);

fs.writeFile('../public/output/noOfWinsEachTeamEachYear.json', data, (err) => {
    if (err) throw err;

});
    }
//third function
function extraRunsPerTeam(matchsobj,deliveriesObj)
   {
     let extraRuns ={};
    for(let index= 0;index< matchsobj.length;index++ )
     {  
      if( matchsobj[index].season === '2016')
       {       
       let matchid = matchsobj[index].id;  

       for(let index2 = 0;index2 < deliveriesObj.length;index2 ++ )
       {
       
         if(matchid === deliveriesObj[index2].match_id)
         {
 
          if(extraRuns[deliveriesObj[index2].bowling_team] === undefined )
          {
          extraRuns[deliveriesObj[index2].bowling_team] =parseInt( deliveriesObj[index2].extra_runs);
         } 
      else{
         extraRuns[deliveriesObj[index2].bowling_team] += parseInt( deliveriesObj[index2].extra_runs);
          }            
       }
    }
}   
}
console.log(extraRuns)
const fs = require('fs');
let data = JSON.stringify(extraRuns, null, 2);
fs.writeFile('../public/output/extraRunsPerTeam.json', data, (err) => {
    if (err) throw err;
});
}

// lastfunction 
function economyOfTopTenBowlersIn2015(matchsobj,deliveriesObj){
    let deliveriesArray= [];

    
    let bowlerBallsRunsArray = [];
    let bowlerEconomy = {};
    for(let index= 0;index< matchsobj.length;index++ )
       {      
         if( matchsobj[index].season == 2015)
          {      for(let index2 = 0;index2 < deliveriesObj.length;index2 ++ )
                   {
                       if(matchsobj[index].id === deliveriesObj[index2].match_id)
                       {
                           let ball ;
                           if(deliveriesObj[index2].wide_runs !== '0'|| deliveriesObj[index2].noball_runs !== '0')
                           {
                               temp =0;
                           }
                           else{ temp =1}
                      deliveriesArray.push([deliveriesObj[index2].bowler,temp,parseInt( deliveriesObj[index2].total_runs)])
                       }
                  }
          }
      }
     
  
     for(let i =0;i<deliveriesArray.length;i++)
     {
         if(bowlerEconomy[deliveriesArray[i][0]] === undefined )
         {
             let name = deliveriesArray[i][0];
             let runs =0;
             let balls = 0;
             for(let j = 0;j<deliveriesArray.length;j++)
             {
                 if(name === deliveriesArray[j][0])
                 {   
                     balls += deliveriesArray[j][1];
                     runs +=  deliveriesArray[j][2];
                 }
             } 
             let economy = (runs/balls)*6;
             bowlerEconomy [name] = economy;
             bowlerBallsRunsArray[bowlerBallsRunsArray.length] =[name,economy]

         }
     }   
      
      bowlerBallsRunsArray.sort(function(a,b){
            return a[1] -b[1];
        })
        bowlerBallsRunsArray= bowlerBallsRunsArray.slice(0,10);
      console.log(bowlerBallsRunsArray); 
      const fs = require('fs');
let data = JSON.stringify(bowlerBallsRunsArray, null, 2);
fs.writeFile('../public/output/economyOfTopTenBowlersIn2015.json', data, (err) => {
    if (err) throw err;
});

    }

   